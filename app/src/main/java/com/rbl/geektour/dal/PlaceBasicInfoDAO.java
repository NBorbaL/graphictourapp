package com.rbl.geektour.dal;

import android.content.Context;

import androidx.annotation.NonNull;

import com.rbl.geektour.interfaces.APICallback;
import com.rbl.geektour.model.PlaceBasicInfo;
import com.rbl.geektour.network.RetrofitInitializer;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceBasicInfoDAO {

    private Context mCtx;

    public PlaceBasicInfoDAO(Context ctx) {
        this.mCtx = ctx;
    }

    public void getPlaceBasicInfo(final int id,
                                  final APICallback<PlaceBasicInfo> callback) {
        Call<PlaceBasicInfo> call = new RetrofitInitializer()
                .graphicTourNetworkInterface()
                .getPlaceBasicInfo(id);
        call.enqueue(new Callback<PlaceBasicInfo>() {
            @Override
            public void onResponse(@NonNull Call<PlaceBasicInfo> call,
                                   @NonNull Response<PlaceBasicInfo> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        ResponseBody retorno = response.errorBody();
                        if (retorno != null) {
                            String errorResponse = retorno.string();
                            callback.onError(errorResponse);
                        }
                    } catch (Exception e) {
                        callback.onError(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PlaceBasicInfo> call, @NonNull Throwable t) {
                if (t instanceof IOException) {
                    // Erro de internet
                    callback.onError(t.getMessage());
                } else {
                    // Erro de conversão
                    callback.onError(t.getMessage());
                }
            }
        });
    }

    public void getAllPlacesBasicInfo(final APICallback<List<PlaceBasicInfo>> callback) {
        Call<List<PlaceBasicInfo>> call = new RetrofitInitializer()
                .graphicTourNetworkInterface()
                .getAllPlacesBasicInfo();
        call.enqueue(new Callback<List<PlaceBasicInfo>>() {
            @Override
            public void onResponse(@NonNull Call<List<PlaceBasicInfo>> call,
                                   @NonNull Response<List<PlaceBasicInfo>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        ResponseBody retorno = response.errorBody();
                        if (retorno != null) {
                            String errorResponse = retorno.string();
                            callback.onError(errorResponse);
                        }
                    } catch (Exception e) {
                        callback.onError(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<PlaceBasicInfo>> call, @NonNull Throwable t) {
                if (t instanceof IOException) {
                    // Erro de internet
                    callback.onError(t.getMessage());
                } else {
                    // Erro de conversão
                    callback.onError(t.getMessage());
                }
            }
        });
    }
}
