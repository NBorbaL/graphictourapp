package com.rbl.geektour.dal;

import android.content.Context;

import androidx.annotation.NonNull;

import com.rbl.geektour.interfaces.APICallback;
import com.rbl.geektour.model.Place;
import com.rbl.geektour.network.RetrofitInitializer;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceDAO {

    private Context mCtx;

    public PlaceDAO(Context ctx) {
        this.mCtx = ctx;
    }

    public void getPlace(final int id,
                         final APICallback<Place> callback) {
        Call<Place> call = new RetrofitInitializer()
                .graphicTourNetworkInterface()
                .getPlace(id);
        call.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(@NonNull Call<Place> call,
                                   @NonNull Response<Place> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        ResponseBody retorno = response.errorBody();
                        if (retorno != null) {
                            String errorResponse = retorno.string();
                            callback.onError(errorResponse);
                        }
                    } catch (Exception e) {
                        callback.onError(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Place> call, @NonNull Throwable t) {
                if (t instanceof IOException) {
                    // Erro de internet
                    callback.onError(t.getMessage());
                } else {
                    // Erro de conversão
                    callback.onError(t.getMessage());
                }
            }
        });
    }

    public void getAllPlaces(final APICallback<List<Place>> callback) {
        Call<List<Place>> call = new RetrofitInitializer()
                .graphicTourNetworkInterface()
                .getAllPlaces();
        call.enqueue(new Callback<List<Place>>() {
            @Override
            public void onResponse(@NonNull Call<List<Place>> call,
                                   @NonNull Response<List<Place>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        ResponseBody retorno = response.errorBody();
                        if (retorno != null) {
                            String errorResponse = retorno.string();
                            callback.onError(errorResponse);
                        }
                    } catch (Exception e) {
                        callback.onError(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Place>> call, @NonNull Throwable t) {
                if (t instanceof IOException) {
                    // Erro de internet
                    callback.onError(t.getMessage());
                } else {
                    // Erro de conversão
                    callback.onError(t.getMessage());
                }
            }
        });
    }
}
