package com.rbl.geektour.interfaces;

public interface APICallback<T> {
    void onSuccess(T t);

    void onError(String error);
}
