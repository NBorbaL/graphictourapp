package com.rbl.geektour.model;

public class Address {

    public Address() {
    }

    private LatLng coordinates;
    private String streetName;
    private String streetNumber;
    private String lineTwo;
    private String zipCode;
    private City city;
}
