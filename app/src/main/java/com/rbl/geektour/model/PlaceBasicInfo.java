package com.rbl.geektour.model;

public class PlaceBasicInfo {
    public final static String TABLE = "PLACES";
    public final static String ID = "ID";
    public final static String NAME = "NAME";
    public final static String DESCRIPTION = "DESCRIPTION";
    public final static String LOGO = "LOGO";

    public PlaceBasicInfo() {
    }

    private int id;
    private String name;
    private String description;
    private Image logo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Image getLogo() {
        return logo;
    }

    public void setLogo(Image logo) {
        this.logo = logo;
    }
}
