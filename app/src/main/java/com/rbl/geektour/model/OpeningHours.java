package com.rbl.geektour.model;

import java.time.DayOfWeek;
import java.util.Date;

public class OpeningHours {

    public OpeningHours(DayOfWeek dayOfWeek,
                        Date from,
                        Date to)
    {
        this.dayOfWeek = dayOfWeek;
        this.from = from;
        this.to = to;
    }

    private DayOfWeek dayOfWeek;
    private Date from;
    private Date to;

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
