package com.rbl.geektour.model;

import java.util.List;

public class Place {

    public Place() {
    }

    private PlaceBasicInfo basicInfo;
    private List<Image> images;
    private List<OpeningHours> openingHours;
    private List<Address> address;
    private List<Phone> phones;
    private List<SocialMedia> socialMediaInfo;

    public PlaceBasicInfo getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(PlaceBasicInfo basicInfo) {
        this.basicInfo = basicInfo;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<OpeningHours> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<OpeningHours> openingHours) {
        this.openingHours = openingHours;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public List<SocialMedia> getSocialMediaInfo() {
        return socialMediaInfo;
    }

    public void setSocialMediaInfo(List<SocialMedia> socialMediaInfo) {
        this.socialMediaInfo = socialMediaInfo;
    }
}
