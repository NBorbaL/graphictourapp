package com.rbl.geektour.model;

import androidx.annotation.Nullable;

public class SocialMedia {

    public enum SocialMediaType {
        FACEBOOK(0),
        TWITTER(1),
        INSTAGRAM(2),
        LINKEDIN(3),
        EMAIL(4),
        OTHER(999);

        private Integer id;

        SocialMediaType(Integer value) {
            this.id = value;
        }

        public Integer getId() {
            return id;
        }

        @Nullable
        public static SocialMediaType fromId(Integer id) {
            for (SocialMediaType at : SocialMediaType.values()) {
                if (at.getId().equals(id)) {
                    return at;
                }
            }
            return null;
        }
    }

    public SocialMedia() {
    }

    private SocialMediaType socialMediaType;
    private String contactInfo;

    public SocialMediaType getSocialMediaType() {
        return socialMediaType;
    }

    public void setSocialMediaType(SocialMediaType socialMediaType) {
        this.socialMediaType = socialMediaType;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }
}
