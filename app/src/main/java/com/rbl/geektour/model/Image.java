package com.rbl.geektour.model;

public class Image {

    public Image() {
    }

    private String source;
    private String cdnId;
    private int order;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCdnId() {
        return cdnId;
    }

    public void setCdnId(String cdnId) {
        this.cdnId = cdnId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
