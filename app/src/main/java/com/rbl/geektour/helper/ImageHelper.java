package com.rbl.geektour.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ImageHelper {

    /**
     * Converte um bitmap para byteArray para passar imagem via intent
     *
     * @param bitmap imagem que sera convertida
     * @return byteArray da imagem
     */
    public static byte[] converteBitmapParaByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Converte um byteArray para bitmap
     *
     * @param byteArray byteArray de uma imagem recebida via intent
     * @return bitmap convertido
     */
    public static Bitmap converteByteArrayParaBitmap(byte[] byteArray) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
            options.inSampleSize = calculateInSampleSize(options, options.outWidth / 2, options.outHeight / 2);
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
        } finally {
            if (byteArray != null) {
                byteArray = null;
                System.gc();
            }
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * reduces the size of the image
     *
     * @param image  uncompressed image
     * @param reduce how much to reduce
     * @return new bitmap
     */
    public static Bitmap reduceBitmap(Bitmap image, int reduce) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width -= reduce;
            height = (int) (width / bitmapRatio);
        } else {
            height -= reduce;
            width = (int) (height * bitmapRatio);
        }
        if (width > 0 && height > 0)
            return Bitmap.createScaledBitmap(image, width, height, true);
        else
            return image;
    }

    public static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxWidth <= 0) {
            maxWidth = image.getWidth();
        }

        if (maxHeight <= 0) {
            maxHeight = image.getHeight();
        }

        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }


    public static void saveImage(Context context, Bitmap bitmap, String name, String extension, int compression) {
        if (compression > 100) {
            compression = 100;
        }

        if (compression < 0) {
            compression = 0;
        }

        name = name + "." + extension;
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 - compression, fileOutputStream);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap loadImageBitmap(Context context, String name, String extension) {
        name = name + "." + extension;
        FileInputStream fileInputStream;
        Bitmap bitmap = null;
        try {
            fileInputStream = context.openFileInput(name);
            bitmap = BitmapFactory.decodeStream(fileInputStream);
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static boolean deleteImage(Context context, String name, String extension) {
        name = name + "." + extension;
        try {
            return context.deleteFile(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static List<String> listaBitmapParaListaBase64(List<Bitmap> bitmapList, boolean limparBitmapsUtilizadas) {
        List<String> byteArrayList = new ArrayList<>();
        for (Bitmap bitmap : bitmapList) {
            byteArrayList.add(Base64.encodeToString(converteBitmapParaByteArray(bitmap), Base64.NO_WRAP));
            if (limparBitmapsUtilizadas) {
                bitmap.recycle();
            }
        }

        if (limparBitmapsUtilizadas) {
            bitmapList.clear();
        }
        return byteArrayList;
    }

    public static List<String> caminhoImagensParaListaBase64(Context ctx, List<String> caminhos, String extension) {
        List<String> byteArrayList = new ArrayList<>();
        for (String caminho : caminhos) {
            Bitmap bmp = ImageHelper.loadImageBitmap(ctx, caminho, extension);
            if (bmp != null) {
                byteArrayList.add(Base64.encodeToString(converteBitmapParaByteArray(bmp), Base64.NO_WRAP));
                bmp.recycle();
                bmp = null;
            }
        }
        return byteArrayList;
    }

    public static void deletarCaminhosImagens(Context ctx, List<String> caminhos, String extensao) {
        for (String caminho : caminhos) {
            deleteImage(ctx, caminho, extensao);
        }
    }

    public static Bitmap base64ParaImagem(String imagemBase64) {
        if (imagemBase64 != null && !imagemBase64.isEmpty()) {
            byte[] byteArray = Base64.decode(imagemBase64, Base64.NO_WRAP);
            return converteByteArrayParaBitmap(byteArray);
        } else {
            return null;
        }
    }
}
