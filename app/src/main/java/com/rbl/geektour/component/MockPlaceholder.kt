package com.rbl.geektour.component

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.rbl.geektour.R

class MockPlaceHolder(context: Context, attrs: AttributeSet?): LinearLayout(context, attrs) {

    var repeat = 1
    var layoutResource = android.R.layout.activity_list_item

    init { init(attrs) }

    private fun init(attrs: AttributeSet?) {
        orientation = VERTICAL

        with (context.theme.obtainStyledAttributes(attrs, R.styleable.MockPlaceHolder, 0, 0)) {
            try {
                repeat = getInteger(R.styleable.MockPlaceHolder_repeat, 1)
                layoutResource = getResourceId(R.styleable.MockPlaceHolder_layoutRes, android.R.layout.activity_list_item)

                for (i in 1..repeat) {
                    addView(inflate(context, layoutResource, null))
                }
            } finally {  recycle() }
        }
    }
}