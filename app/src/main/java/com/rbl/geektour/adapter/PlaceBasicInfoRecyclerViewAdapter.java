package com.rbl.geektour.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cloudinary.android.MediaManager;
import com.rbl.geektour.R;
import com.rbl.geektour.model.PlaceBasicInfo;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Criado por renan.lucas em 03/04/2018.
 */

public class PlaceBasicInfoRecyclerViewAdapter extends RecyclerView.Adapter<PlaceBasicInfoRecyclerViewAdapter.ViewHolder> {

    private Context ctx;
    private List<PlaceBasicInfo> placeBasicInfoList;
    private View.OnClickListener onClickListener;

    public PlaceBasicInfoRecyclerViewAdapter(Context ctx, List<PlaceBasicInfo> placeBasicInfoList,
                                             View.OnClickListener onClickListener) {
        this.ctx = ctx;
        this.placeBasicInfoList = placeBasicInfoList;
        this.onClickListener = onClickListener;
        try {
            MediaManager.init(ctx);
        } catch (Exception ignored) {
        }
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_place_basic_info, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        if (onClickListener != null) {
            viewHolder.itemView.setOnClickListener(onClickListener);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        TextView textViewName = holder.textViewName;
        TextView textViewDescription = holder.textViewDescription;
        final CircleImageView circleImageViewLogo = holder.circleImageViewLogo;

        // Gets the current item for the specified position
        PlaceBasicInfo placeBasicInfo = placeBasicInfoList.get(position);
        holder.itemView.setTag(placeBasicInfo);

        // Sets the info on the holder items
        textViewName.setText(placeBasicInfo.getName());
        textViewDescription.setText(placeBasicInfo.getDescription());

        if (placeBasicInfo.getLogo() != null) {
            final String imageCdnId = placeBasicInfo.getLogo().getCdnId();
            if (imageCdnId != null && !imageCdnId.isEmpty()) {
                circleImageViewLogo.post(new Runnable() {
                    @Override
                    public void run() {
                        // Generates a Cloudinary custom url with the ImageView's width and height
                        // and loads it into our imageview with Glide
                        int width = circleImageViewLogo.getMeasuredWidth();
                        int height = circleImageViewLogo.getMeasuredHeight();
                        String url = MediaManager.get().url().secure(true).publicId(imageCdnId).generate();
                        Glide.with(ctx)
                                .load(url)
                                .override(width, height)
                                .placeholder(R.drawable.place_logo_placeholder)
                                .error(R.drawable.place_logo_placeholder)
                                .into(circleImageViewLogo);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return placeBasicInfoList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewDescription;
        CircleImageView circleImageViewLogo;

        private ViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.CardPlaceBasicInfo_TextViewPlaceName);
            this.textViewDescription = itemView.findViewById(R.id.CardPlaceBasicInfo_TextViewPlaceDescription);
            this.circleImageViewLogo = itemView.findViewById(R.id.CardPlaceBasicInfo_CircleImageViewLogo);
        }
    }
}