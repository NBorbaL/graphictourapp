package com.rbl.geektour.controller;

import android.content.Context;

import com.rbl.geektour.dal.PlaceBasicInfoDAO;
import com.rbl.geektour.interfaces.APICallback;
import com.rbl.geektour.model.PlaceBasicInfo;

import java.util.List;

public class PlaceBasicInfoController {

    private PlaceBasicInfoDAO placeBasicInfoDAO;

    public PlaceBasicInfoController(Context ctx) {
        placeBasicInfoDAO = new PlaceBasicInfoDAO(ctx);
    }

    public void getPlaceBasicInfo(final int id, final APICallback<PlaceBasicInfo> callback) {
        placeBasicInfoDAO.getPlaceBasicInfo(id, callback);
    }

    public void getAllPlacesBasicInfo(final APICallback<List<PlaceBasicInfo>> callback) {
        placeBasicInfoDAO.getAllPlacesBasicInfo(callback);
    }
}
