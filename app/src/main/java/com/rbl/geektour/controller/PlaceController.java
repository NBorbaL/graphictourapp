package com.rbl.geektour.controller;

import android.content.Context;

import com.rbl.geektour.dal.PlaceDAO;
import com.rbl.geektour.interfaces.APICallback;
import com.rbl.geektour.model.Place;

import java.util.List;

public class PlaceController {

    private PlaceDAO placeDAO;

    public PlaceController(Context ctx) {
        placeDAO = new PlaceDAO(ctx);
    }

    public void getPlace(final int id, final APICallback<Place> callback) {
        placeDAO.getPlace(id, callback);
    }

    public void getAllPlaces(final APICallback<List<Place>> callback) {
        placeDAO.getAllPlaces(callback);
    }
}
