package com.rbl.geektour.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.rbl.geektour.R;
import com.rbl.geektour.adapter.PlaceBasicInfoRecyclerViewAdapter;
import com.rbl.geektour.component.MockPlaceHolder;
import com.rbl.geektour.controller.PlaceController;
import com.rbl.geektour.interfaces.APICallback;
import com.rbl.geektour.model.Place;
import com.rbl.geektour.model.PlaceBasicInfo;

import java.util.ArrayList;
import java.util.List;

public class PlaceDetailActivity extends AppCompatActivity {

    private PlaceController placeController = new PlaceController(this);

    private int placeId = 0;

    // Views
    private MockPlaceHolder mockPlaceHolder;
    private SwipeRefreshLayout pullToRefresh;
    private RecyclerView recyclerViewPlacesBasicInfo;

    private LinearLayout linearLayoutConnectionError;
    private Button buttonRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        Intent intent = getIntent();
        if (intent != null
                && intent.hasExtra("placeId")) {
            placeId = intent.getIntExtra("placeId", 0);
        }

        if (placeId > 0) {
            findViewsById();
            setupButtonListeners();
            setupPullToRefresh();
            getAndShowAllPlaceBasicInfo(placeId);
        } else {
            Toast.makeText(this, getString(R.string.message_incorrect_parameters), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAndShowAllPlaceBasicInfo(placeId);
    }

    private void findViewsById() {
        mockPlaceHolder = findViewById(R.id.PlacesActivity_MockPlaceHolder);
        pullToRefresh = findViewById(R.id.PlacesActivity_SwipeRefreshLayoutRecyclerView);
        recyclerViewPlacesBasicInfo = findViewById(R.id.PlacesActivity_RecyclerViewPlacesBasicInfo);
        linearLayoutConnectionError = findViewById(R.id.PlacesActivity_LinearLayoutConnectionError);
        buttonRetry = findViewById(R.id.PlacesActivity_ButtonRetry);
    }

    private void setupButtonListeners() {
        buttonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAndShowAllPlaceBasicInfo(placeId);
            }
        });
    }

    private void setupPullToRefresh() {
        pullToRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAndShowAllPlaceBasicInfo(placeId);
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void getAndShowAllPlaceBasicInfo(int id) {
        try {
            recyclerViewPlacesBasicInfo.setVisibility(View.GONE);
            recyclerViewPlacesBasicInfo.setAdapter(null);
            toggleConnectionErrorLinearLayout(false);
            toggleMockPlaceholder(true);
            placeController.getPlace(id, new APICallback<Place>() {
                @Override
                public void onSuccess(final Place place) {
                    if (place != null) {
                        toggleConnectionErrorLinearLayout(false);
                        toggleMockPlaceholder(false);
                        List<PlaceBasicInfo> placeBasicInfoList = new ArrayList<>();
                        placeBasicInfoList.add(place.getBasicInfo());
                        showAllPlacesBasicInfo(placeBasicInfoList);
                    }
                }

                @Override
                public void onError(String error) {
                    toggleConnectionErrorLinearLayout(true);
                    toggleMockPlaceholder(false);
                }
            });
        } catch (Exception e) {
            toggleConnectionErrorLinearLayout(true);
            toggleMockPlaceholder(false);
        }
    }

    private void showAllPlacesBasicInfo(List<PlaceBasicInfo> placeBasicInfoList) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewPlacesBasicInfo.setHasFixedSize(true);
        recyclerViewPlacesBasicInfo.setLayoutManager(layoutManager);
        recyclerViewPlacesBasicInfo.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPlacesBasicInfo.setItemViewCacheSize(20);

        // Adapts content to cards
        PlaceBasicInfoRecyclerViewAdapter placeBasicInfoRecyclerViewAdapter = new PlaceBasicInfoRecyclerViewAdapter(this,
                placeBasicInfoList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlaceBasicInfo clickedPlace = (PlaceBasicInfo) view.getTag();
                Toast.makeText(PlaceDetailActivity.this, "User clicked on: " + clickedPlace.getId(), Toast.LENGTH_LONG).show();
            }
        });
        recyclerViewPlacesBasicInfo.setAdapter(placeBasicInfoRecyclerViewAdapter);
        recyclerViewPlacesBasicInfo.setVisibility(View.VISIBLE);
    }

    private void toggleMockPlaceholder(boolean visible) {
        mockPlaceHolder.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void toggleConnectionErrorLinearLayout(boolean visible) {
        linearLayoutConnectionError.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
