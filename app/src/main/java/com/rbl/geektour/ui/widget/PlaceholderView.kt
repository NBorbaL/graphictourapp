package com.rbl.geektour.ui.widget;

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import com.rbl.geektour.R

class PlaceHolderView(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    init {
        setBackgroundResource(R.drawable.loading_placeholder)
        startAnimation(AnimationUtils.loadAnimation(context, R.anim.placeholder))
    }
}