package com.rbl.geektour.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.rbl.geektour.R;
import com.rbl.geektour.adapter.PlaceBasicInfoRecyclerViewAdapter;
import com.rbl.geektour.component.MockPlaceHolder;
import com.rbl.geektour.controller.PlaceBasicInfoController;
import com.rbl.geektour.interfaces.APICallback;
import com.rbl.geektour.model.PlaceBasicInfo;

import java.util.List;

public class PlacesActivity extends AppCompatActivity {

    private PlaceBasicInfoController placeBasicInfoController = new PlaceBasicInfoController(this);

    // Views
    private MockPlaceHolder mockPlaceHolder;
    private SwipeRefreshLayout pullToRefresh;
    private RecyclerView recyclerViewPlacesBasicInfo;

    private LinearLayout linearLayoutConnectionError;
    private TextView textViewRetryText;
    private Button buttonRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        findViewsById();
        setupButtonListeners();
        setupPullToRefresh();
        getAndShowAllPlacesBasicInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAndShowAllPlacesBasicInfo();
    }

    private void findViewsById() {
        mockPlaceHolder = findViewById(R.id.PlacesActivity_MockPlaceHolder);
        pullToRefresh = findViewById(R.id.PlacesActivity_SwipeRefreshLayoutRecyclerView);
        recyclerViewPlacesBasicInfo = findViewById(R.id.PlacesActivity_RecyclerViewPlacesBasicInfo);
        linearLayoutConnectionError = findViewById(R.id.PlacesActivity_LinearLayoutConnectionError);
        textViewRetryText = findViewById(R.id.PlacesActivity_TextViewRetryText);
        buttonRetry = findViewById(R.id.PlacesActivity_ButtonRetry);
    }

    private void setupButtonListeners() {
        buttonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAndShowAllPlacesBasicInfo();
            }
        });
    }

    private void setupPullToRefresh() {
        pullToRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(true);
                getAndShowAllPlacesBasicInfo();
            }
        });
    }

    private void getAndShowAllPlacesBasicInfo() {
        try {
            recyclerViewPlacesBasicInfo.setVisibility(View.GONE);
            recyclerViewPlacesBasicInfo.setAdapter(null);
            toggleConnectionErrorLinearLayout(false);
            toggleMockPlaceholder(true);
            placeBasicInfoController.getAllPlacesBasicInfo(new APICallback<List<PlaceBasicInfo>>() {
                @Override
                public void onSuccess(List<PlaceBasicInfo> placeBasicInfos) {
                    if (placeBasicInfos == null || placeBasicInfos.size() <= 0) {
                        toggleConnectionErrorLinearLayout(true, R.string.text_nothing_to_see_here);
                        toggleMockPlaceholder(false);
                        pullToRefresh.setRefreshing(false);
                    } else {
                        toggleConnectionErrorLinearLayout(false);
                        toggleMockPlaceholder(false);
                        pullToRefresh.setRefreshing(false);
                        showAllPlacesBasicInfo(placeBasicInfos);
                    }
                }

                @Override
                public void onError(String error) {
                    toggleConnectionErrorLinearLayout(true, R.string.text_connection_error);
                    toggleMockPlaceholder(false);
                    pullToRefresh.setRefreshing(false);
                }
            });
        } catch (Exception e) {
            toggleConnectionErrorLinearLayout(true, R.string.text_connection_error);
            toggleMockPlaceholder(false);
            pullToRefresh.setRefreshing(false);
        }
    }

    private void showAllPlacesBasicInfo(List<PlaceBasicInfo> placeBasicInfoList) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewPlacesBasicInfo.setHasFixedSize(true);
        recyclerViewPlacesBasicInfo.setLayoutManager(layoutManager);
        recyclerViewPlacesBasicInfo.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPlacesBasicInfo.setItemViewCacheSize(20);

        // Adapts content to cards
        PlaceBasicInfoRecyclerViewAdapter placeBasicInfoRecyclerViewAdapter = new PlaceBasicInfoRecyclerViewAdapter(this,
                placeBasicInfoList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlaceBasicInfo clickedPlace = (PlaceBasicInfo) view.getTag();
                Intent intent = new Intent(PlacesActivity.this, PlaceDetailActivity.class);
                intent.putExtra("placeId", clickedPlace.getId());
                startActivity(intent);
            }
        });
        recyclerViewPlacesBasicInfo.setAdapter(placeBasicInfoRecyclerViewAdapter);
        recyclerViewPlacesBasicInfo.setVisibility(View.VISIBLE);
    }

    private void toggleMockPlaceholder(boolean visible) {
        mockPlaceHolder.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void toggleConnectionErrorLinearLayout(boolean visible) {
        textViewRetryText.setText(getString(R.string.text_connection_error));
        linearLayoutConnectionError.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void toggleConnectionErrorLinearLayout(boolean visible, int resId) {
        textViewRetryText.setText(getString(resId));
        linearLayoutConnectionError.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
