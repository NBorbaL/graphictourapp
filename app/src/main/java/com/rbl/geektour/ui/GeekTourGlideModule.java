package com.rbl.geektour.ui;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

// new since Glide v4
@GlideModule
public final class GeekTourGlideModule extends AppGlideModule {
    // leave empty for now
}