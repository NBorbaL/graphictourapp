package com.rbl.geektour.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by renanlucas on 08/03/18.
 */
class RetrofitInitializer {

    private val BASE_URL = "https://graphictourapi.azurewebsites.net/api/"

    private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun graphicTourNetworkInterface() = retrofit.create(GeekTourNetworkInterface::class.java)
}