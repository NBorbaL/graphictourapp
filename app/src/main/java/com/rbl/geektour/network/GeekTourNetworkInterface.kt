package com.rbl.geektour.network;

import com.rbl.geektour.model.Place
import com.rbl.geektour.model.PlaceBasicInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by renanlucas on 08/03/18.
 */
interface GeekTourNetworkInterface {
    @GET("PlaceBasicInfo")
    fun getAllPlacesBasicInfo(): Call<List<PlaceBasicInfo>>

    @GET("PlaceBasicInfo/{id}")
    fun getPlaceBasicInfo(@Path("id") id: Int): Call<PlaceBasicInfo>

    @GET("Place")
    fun getAllPlaces(): Call<List<Place>>

    @GET("Place/{id}")
    fun getPlace(@Path("id") id: Int): Call<Place>
}